import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch, withRouter} from 'react-router-dom';
import {Container} from 'react-materialize';
import $ from 'jquery';
import 'materialize-css/dist/js/materialize.min';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import 'public/css/style.scss';

import Navigation from './components/Navigation';
import Footer from './components/Footer';
import Home from './components/Home';
import Information from './components/Information';
import Slider from './components/SliderShow';
import Towns from './components/Towns';
import TownInfo from './components/TownInfo';

import towns from "./townsList";

class App extends React.Component{
    constructor(props) {
        super(props);

        this.handleFilter = this.handleFilter.bind(this);
        this.state = {
            towns: [],
        };
    }

    handleFilter(townName){
        let distinctTowns = [];
        let townsList = towns.filter((town, index) => {
            if(!distinctTowns.includes(town.townName)){
                distinctTowns.push(town.townName);
                if(town.townName.toLowerCase().includes(townName.toLowerCase())) return town
            }
        });

        this.setState({
            towns: townsList,
        });
    }

    componentDidMount(){
        let townsList = towns.filter((town, index) => { 
            if(index === 0) return town;
            return town.townName !== towns[index-1].townName;
        });
        
        this.setState({
            towns: townsList
        });
    }

    render(){
        return (
            <Router>
                <div>
                    <Navigation />
                    <Route exact path={"/"} component={Slider} />
                    <div className="container row">
                    <Route path={"/towns-list"} render={(props) => <Towns filterTown={this.handleFilter} {...props} townsList={this.state.towns} /> } />
                        <Switch>
                            <Route exact path={"/"} component={Home} />
                            <Route path={"/information"} component={Information} />
                            <Route 
                                path={"/towns-list/:town"} render={(props) => <TownInfo {...props} />} 
                            />
                        </Switch>
                    </div>
                    <Footer />
                </div>
            </Router>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));