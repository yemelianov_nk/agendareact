import React from 'react';
import OneCard from './OneCard';

function Home() {
    return(
        <div className="home-container">
            <div className="cards-container">
                <div className="row">
                    <div className="col s12 home-cards-container">
                        <div className="row">
                            <OneCard
                                title="Card Title"
                                cardText="I am a very simple card. I am good at containing small bits of information.
                                    I am convenient because I require little markup to use effectively."
                                linkText="This is a link"
                            />
                            <OneCard
                                title="Card Title"
                                cardText="I am a very simple card. I am good at containing small bits of information.
                                    I am convenient because I require little markup to use effectively."
                                linkText="This is a link"
                            />
                            <OneCard
                                title="Card Title"
                                cardText="I am a very simple card. I am good at containing small bits of information.
                                    I am convenient because I require little markup to use effectively."
                                linkText="This is a link"
                            />
                            <OneCard
                                title="Card Title"
                                cardText="I am a very simple card. I am good at containing small bits of information.
                                    I am convenient because I require little markup to use effectively."
                                linkText="This is a link"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Home;