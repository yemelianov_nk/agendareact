import React from 'react';
import { NavLink } from 'react-router-dom';
import {Navbar, NavItem, Icon, Dropdown} from 'react-materialize';


function Navigation() {
    return(
        <Navbar brand='logo' right>
            <li>
                <NavLink to="/"><Icon>home</Icon></NavLink>
            </li>
            <li>
                <NavLink to="/towns-list">Towns</NavLink>
            </li>
            <li>
                <NavLink to="/information"><Icon>info_outline</Icon></NavLink>
            </li>
        </Navbar>
    );
}

export default Navigation;