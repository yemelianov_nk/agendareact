import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import {Collection, CollectionItem} from 'react-materialize';

function TownsCollections(props){
    return(
        <div className="collection">
            {props.townsList.map(town =>
                <Link key={town.id} to={`${props.match.url}/${town.townName}`} className={`collection-item ${props.className ? 'active' : ''}`} onClick={props.toggleActiveClass}>{town.townName}</Link>
            )
            }
        </div>
    );
}

TownsCollections.propTypes = {
    townsList: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        townName: PropTypes.string.isRequired,
    })).isRequired
};

export default TownsCollections;