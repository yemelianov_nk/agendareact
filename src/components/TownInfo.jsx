import React from 'react';
import { Route, withRouter } from 'react-router-dom';
import { Row } from 'react-materialize';

import dateFormat from 'dateformat';
import frenchMonths from '../fr/month';
import weekDays from '../fr/days';

dateFormat.i18n = {
    dayNames: weekDays,
    monthNames: frenchMonths,
    timeNames: [
        'a', 'p', 'am', 'pm', 'A', 'P', 'AM', 'PM'
    ]
};

import Center from './Center';
import FilterCenter from './FilterCenter';
import townsInfo from '../townsList';

class TownInfo extends React.Component{
    constructor(props){
        super(props);

        $(document).ready(function() {
            $('.collapsible').collapsible();
        });

        this.state = {
            townInfo: [],
            town: this.props.match.params.town,
            dateStart: '',
            dateEnd: ''
        };

        this.handleFilterCenter = this.handleFilterCenter.bind(this);
    }

    componentDidMount(){
        let townInfo = townsInfo.filter(town => town.townName.toLowerCase().includes(this.state.town.toLowerCase()));

        this.setState({
            townInfo
        });
    }

    componentWillReceiveProps(nextProps){
        let townName = nextProps.match.params.town;
        let townInfo = townsInfo.filter(town => town.townName.toLowerCase().includes(townName.toLowerCase()));
        this.setState({
            town: townName,
            townInfo
        })
    }

    handleFilterCenter(date, id){
        switch(id){
            case 'date-start':
                this.setState({ dateStart:  date});
                break;
            case 'date-end':
                this.setState({ dateEnd:  date});
                break;
        }

        let dateStart = this.state.dateStart;
        let dateEnd = this.state.dateEnd;

        let centersByDate = townsInfo.filter(town =>{
            if(town.townName.toLowerCase() == this.state.town.toLowerCase()){
                if(this.state.dateStart !== '' || this.state.dateEnd !== ''){
                        let date = dateFormat(new Date(town.date), "yyyy-mm-dd");
                        if(dateStart !== '' && dateEnd!== '' &&  date >= dateStart && date <= dateEnd){
                            return town;
                        }else if(dateEnd === '' && dateStart !== '' && date >= dateStart){
                            return town;
                        }else if(dateStart === '' && dateEnd === '' && date <= dateEnd){
                            return town;
                        }

                }else{
                    return town;
                }
            }
        });

        this.setState({ townInfo: centersByDate });
    }

    render() {
        console.log();
        return (
            <div  className="col m9 s12 center-container">
                <Row>
                    <FilterCenter placeholder={"Du"} id={"date-start"} handleFilterCenter={this.handleFilterCenter}/>
                    <FilterCenter placeholder={"Au"} id={"date-end"} handleFilterCenter={this.handleFilterCenter}/>
                </Row>

                <div className="center-list-container">
                    <ul className="collapsible" data-collapsible="accordion">
                        {
                           this.state.townInfo.map(town =>
                             <Center key={town.id} centersInfo={town}/>
                         )
                     }
                </ul>
                </div>
            </div>
        );
    }
}

export default TownInfo;