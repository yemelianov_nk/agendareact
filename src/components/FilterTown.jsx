import React from 'react';

class FilterTown extends React.Component{
    constructor(props){
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.state = {
            townName: '',
        };
    }

    componentWillReceiveProps(netxProps){
        if(this.props.townName !== netxProps.townName){
            this.setState({
                townName: netxProps.townName
            });
        }
    }

    handleChange(event){
        let title = event.target.value;
        this.props.filterTown(title);
        this.setState({
            townName: title
        });
    }

    render(){
        return(
            <div className="input-field">
                <input id="townName" type="text" onChange={this.handleChange} onFocus={this.value = this.value} value={this.state.townName}/>
                <label htmlFor="townNameme" className={this.state.townName !== '' ? 'active' : ''}>Town</label>
            </div>
        );
    }
}

export default FilterTown;