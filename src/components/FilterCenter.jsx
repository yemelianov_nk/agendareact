import React from 'react';
import { Input } from 'react-materialize';

import dateFormat from 'dateformat';
import frenchMonths from '../fr/month';
import weekDays from '../fr/days';

dateFormat.i18n = {
    dayNames: weekDays,
    monthNames: frenchMonths,
    timeNames: [
        'a', 'p', 'am', 'pm', 'A', 'P', 'AM', 'PM'
    ]
};

class FilterCenter extends React.Component{
    constructor(props){
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event){
        let date = new Date(event.target.value);
        date = dateFormat(date, 'yyyy-mm-dd');
        let id = this.props.id;

        this.props.handleFilterCenter(date, id);
    }

    render(){
        return(
            <Input s={12} m={6} id={this.props.id} name='on' placeholder={this.props.placeholder} type='date' onChange={this.handleChange} />
        );
    }
}

export default FilterCenter;