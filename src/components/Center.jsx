import React from 'react';
import dateFormat from 'dateformat';
import {Row, Input, Icon, Button} from 'react-materialize';

import frenchMonths from '../fr/month';
import weekDays from '../fr/days';

dateFormat.i18n = {
    dayNames: weekDays,
    monthNames: frenchMonths,
    timeNames: [
        'a', 'p', 'am', 'pm', 'A', 'P', 'AM', 'PM'
    ]
};

class Center extends React.Component{
    constructor(props){
        super(props);

        this.transformDate = this.transformDate.bind(this);
    }

    transformDate(centerDate){
        let date = new Date(centerDate);

        return dateFormat(date, 'dd mmmm yyyy')
    }

    render(){
        return (
            <li>
                <div className="collapsible-header">
                    <div className="row short-dicribe-container">
                        <div className="col m1">
                            <Icon>home</Icon>
                        </div>
                        <div className="col m9 app-info">
                            <div className="row">
                                {`${this.transformDate(this.props.centersInfo.date)}`} 
                            </div>
                            <div className="row">
                                Center: {`${this.props.centersInfo.centerName}, ${this.props.centersInfo.centerAddress}`}
                            </div>
                            <div className="row">
                                Time: {`${this.props.centersInfo.timeStart}-${this.props.centersInfo.timeEnd}`}
                            </div> 
                        </div>
                        <div className="col m2 price-container">
                        {`${this.props.centersInfo.price} $`}
                        </div>
                    </div>
                </div>
                <div className="collapsible-body">
                    <Row>
                    	<Input s={12} m={6} id="first_name" label="First Name"><Icon>account_circle</Icon></Input>
                    	<Input s={12} m={6} id="last_name" label="Last Name"><Icon>account_circle</Icon></Input>
                    	<Input type="email" label="Email" validate s={12} m={6}><Icon>email</Icon></Input>
                    	<Input type="email" label="Telephone" s={12} m={6}><Icon>phone</Icon></Input>
                    </Row>
                    <div className="row">
                        <div className="s12 button-container">
                            <Button>Send<Icon right>send</Icon></Button>
                        </div>
                    </div>
                </div>
            </li>
        );
    }
}

export default Center;