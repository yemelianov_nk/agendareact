import React from 'react';

function Cards(props) {
    return(
        <div className="col s12 m6">
            <div className="card darken-1">
                <div className="card-content white-text">
                    <span className="card-title">{props.title}</span>
                    <p>{props.cardText}</p>
                </div>
                <div className="card-action">
                    <a href="#">{props.linkText}</a>
                </div>
            </div>
        </div>

    );
}

export default Cards;