import React from 'react';
import { Route, withRouter } from 'react-router-dom';
import TownsCollections from './TownsCollections';
import FilterTown from './FilterTown';


class Towns extends React.Component{
    constructor(props){
        super(props);

        this.toggleActiveClass= this.toggleActiveClass.bind(this);
        this.state = {
            active: false,
            townName: ''
        };

    }

    componentDidMount(){
        this.props.filterTown('');
    }

    toggleActiveClass(event) {
        let triggerLink = event.target;
        let parentDiv = triggerLink.parentNode.querySelectorAll('a');
        for (let link of parentDiv){
            link.classList.remove('active');
        }
        triggerLink.classList.add('active');

        this.setState({
            townName: triggerLink.textContent
        });

        this.props.filterTown(triggerLink.textContent);
    };

    render(){
        return(
            <div className="col s12 m3 town-list-container">
                <div className="row">
                    <div className="filter-container col s12">
                        <div className="search-input-container">
                            <FilterTown filterTown={this.props.filterTown} townName={this.state.townName}/>
                        </div>
                    </div>
                </div>
                <div className="row towns-collection-container">
                    <div className="col s12">
                        <TownsCollections
                            townsList={this.props.townsList} 
                            match={this.props.match}
                            toggleActiveClass={this.toggleActiveClass} 
                        />
                    </div>
                </div>
            </div>

        );
    }
}



export default Towns;