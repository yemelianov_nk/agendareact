import React from 'react';
import {Tabs, Tab} from 'react-materialize';

function Information() {
    return(
        <div className="row about-tabs-container">
            <div className="col s12">
                <Tabs className='tab-demo z-depth-1 tabs-about'>
                    <Tab title="Réglementation" active>
                        <div className="col s12">
                             <img className="responsive-img" src="/img/resatest-visite-medicale.jpg" />
                        </div>
                        <div className="col s12">
                            <p>
                                Afin de solliciter un nouveau permis de conduire après une annulation, 
                                suspension ou invalidation du permis de conduire, les conducteurs doivent 
                                recevoir un avis médical après avoir passé la visite médicale du permis de 
                                conduire et un examen psychotechnique (article R224-21 du Code de la route).
                            </p>
                            <p>
                                La visite médicale s’effectue aux frais du conducteur.
                            </p>
                         </div>
                    </Tab>
                    <Tab title="Test 2">
                        <div className="col s12">
                            <p>
                                Afin de solliciter un nouveau permis de conduire après une annulation, 
                                suspension ou invalidation du permis de conduire, les conducteurs doivent 
                                recevoir un avis médical après avoir passé la visite médicale du permis de 
                                conduire et un examen psychotechnique (article R224-21 du Code de la route).
                            </p>
                            <p>
                                La visite médicale s’effectue aux frais du conducteur.
                            </p>
                         </div>
                    </Tab>
                </Tabs>
            </div>
        </div>

    );
}

export default Information;