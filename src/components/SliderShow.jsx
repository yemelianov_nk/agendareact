import React from 'react';
import {Slider, Slide} from 'react-materialize';

function SliderShow() {
    return(
        <div className="slider-container">
            <Slider indicators={false}>
                <Slide
                    src="/img/Doctor-page.png"
                    title="This is our big Tagline!">
                    Here's our small slogan.
                </Slide>
                <Slide
                    src="/img/Doctor-page_06.jpg"
                    title="Left aligned Caption"
                    placement="left">
                    Here's our small slogan.
                </Slide>
                <Slide
                    src="/img/Photo.png"
                    title="Right aligned Caption"
                    placement="right">
                    Here's our small slogan.
                </Slide>
            </Slider>
        </div>
    );
}

export default SliderShow;