const townsList = [
    {
        id: 1,
        townName: 'Paris',
        centerName: 'Multiburo',
        centerAddress: '52 bld Sébastopol',
        date: '2017-12-10',
        timeStart: '13:00',
        timeEnd: '14:00',
        price: '90'
    } ,
    {
        id: 2,
        townName: 'Paris',
        centerName: 'AFR Centre d\'Affaires',
        centerAddress: '2 bld de la Libération',
        date: '2017-11-01',
        timeStart: '13:00',
        timeEnd: '14:00',
        price: '90'
    } ,
    {
        id: 3,
        townName: 'Paris',
        centerName: 'NCI',
        centerAddress: '4 bd St Antoine',
        date: '2017-12-22',
        timeStart: '13:00',
        timeEnd: '14:00',
        price: '90'
    } ,
    {
        id: 4,
        townName: 'Paris',
        centerName: 'Perinord',
        centerAddress: '2 bld de la Libération',
        date: '2017-12-22',
        timeStart: '13:00',
        timeEnd: '14:00',
        price: '90'
    } ,
    {
        id: 5,
        townName: 'Leon',
        centerName: 'Perinord',
        centerAddress: '52 bld Sébastopol',
        date: '2017-12-22',
        timeStart: '13:00',
        timeEnd: '14:00',
        price: '90'
    },
    {
        id: 6,
        townName: 'Leon',
        centerName: 'NCI',
        centerAddress: '2 bld de la Libération',
        date: '2017-11-01',
        timeStart: '13:00',
        timeEnd: '14:00',
        price: '90'
    },
    {
        id: 7,
        townName: 'Nice',
        centerName: 'Perinord',
        centerAddress: '52 bld Sébastopol',
        date: '2017-11-01',
        timeStart: '13:00',
        timeEnd: '14:00',
        price: '90'
    },
    {
        id: 8,
        townName: 'Praga',
        centerName: 'Perinord',
        centerAddress: '52 bld Sébastopol',
        date: '2017-12-10',
        timeStart: '13:00',
        timeEnd: '14:00',
        price: '90'
    },
    {
        id: 9,
        townName: 'Lviv',
        centerName: 'Perinord',
        centerAddress: '2 bld de la Libération',
        date: '2017-10-10',
        timeStart: '13:00',
        timeEnd: '14:00',
        price: '90'
    },
];

export default townsList;