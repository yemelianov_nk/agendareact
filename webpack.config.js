const webpack = require('webpack');

module.exports = {

    entry: [
        "./src/App.jsx",
        "webpack-dev-server/client?http://localhost:3000/",
        "webpack/hot/only-dev-server"
    ],

    output: {
        filename: "bundle.js",
        path: __dirname + '/public'
    },
    devServer: {
        contentBase: './public',
        port: 3000,
        historyApiFallback: true
    },

    // Bundle lookup dir for included/imported modules
    // By default, bundler/webpack with search here for the scripts
    resolve: {
        modulesDirectories: ['node_modules', 'src'],
        extensions: ['', '.js', '.jsx']
    },
    resolve: {
        modules: [__dirname, 'node_modules'],
        extensions: ['*','.js','.jsx', '.css', '.scss']
    },

    // make sure you added babel-loader to the package
    // npm i babel-loader babel-core babel-preset-es2015 -D
    module: {
        loaders: [
            {
                test: /\.js[x]?$/, // Allowing .jsx file to be transpiled by babel
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            },
            { test: /\.css$/, loaders: [
                'style-loader',
                'css-loader?importLoaders=1',
                'font-loader?format[]=truetype&format[]=woff&format[]=embedded-opentype'
            ] }
            ,
            { test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                loader: 'file-loader?name=fonts/[name].[ext]'
            },
            {
                test: /\.(scss|sass)$/,
                loaders: ["style-loader", "css-loader", "sass-loader"]
            },
        ],

    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
    ]

};